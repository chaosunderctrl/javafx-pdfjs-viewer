package chaosctrl.pdf_viewer;

/**
 * Main class
 * 
 * Separating Main and App classes prevents JavaFX error when executing the distribution.
 * See https://github.com/javafxports/openjdk-jfx/issues/236#issuecomment-426583174
 */
public class Main {
    public static void main(String[] args) {
        App.main(args);
    }
}
