package chaosctrl.pdf_viewer;

import java.io.IOException;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Main app class
 */
public class App extends Application {

    private static Stage mainStage;

    public void start(Stage stage) throws IOException {
        mainStage = stage;
        Parent root = FXMLLoader.load(getClass().getResource("/main.fxml"));
        
        Scene scene = new Scene(root, 800, 600);
        stage.setTitle("PDF Viewer");
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }

    public static Stage getMainStage() {
        return mainStage;
    }
}
