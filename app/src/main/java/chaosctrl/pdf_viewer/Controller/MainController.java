package chaosctrl.pdf_viewer.Controller;

import java.io.File;

import chaosctrl.pdf_viewer.App;

import org.tinylog.Logger;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.stage.FileChooser;

/**
 * Main controller
 */
public class MainController {

    @FXML
    private TabPane tabPane;

    private PdfController pdfController;

    @FXML
    void onQuit(ActionEvent event) {
        closeFile();
        Platform.exit();
    }

    @FXML
    void onOpenFile(ActionEvent event) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("PDF", "*.pdf","*.PDF"));
        File file = fileChooser.showOpenDialog(App.getMainStage());
        if (file == null) {
            return;
        }
        try {
            // Only one PDF instance will be opened at once
            if (pdfController == null) {
                Tab tab = new Tab(file.getName());
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/pdf.fxml"));
                Parent pdfRoot = (Parent)loader.load();
                pdfController = (PdfController)loader.getController();
                tab.setContent(pdfRoot);
                tabPane.getTabs().add(tab);
            } else {
                Tab tab = tabPane.getSelectionModel().getSelectedItem();
                tab.setText(file.getName());
            }

            pdfController.loadPdf(file);
        } catch (Exception e) {
            Logger.error(e);
            MsgController.createAlertForException(e).showAndWait();
        }
    }

    @FXML
    void onCloseFile(ActionEvent event) {
        closeFile();
    }

    private void closeFile() {
        if (pdfController != null) {
            pdfController.unloadAndClose();
            pdfController = null;
        }
        Tab tab = tabPane.getSelectionModel().getSelectedItem();
        if (tab != null) {
            tab.setContent(null);
            tabPane.getTabs().remove(tab);
        }
    }
}
