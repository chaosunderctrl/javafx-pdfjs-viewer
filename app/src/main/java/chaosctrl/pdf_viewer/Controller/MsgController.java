package chaosctrl.pdf_viewer.Controller;

import java.io.PrintWriter;
import java.io.StringWriter;

import javafx.scene.control.Alert;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;

public class MsgController {
    
    /**
     * Creates Alert with given Exception.
     * 
     * @param e The Exception
     * @return The Alert
     */
    public static Alert createAlertForException(Exception e) {
        StringWriter sw = new StringWriter();
        e.printStackTrace(new PrintWriter(sw));
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setHeaderText("Error");
        alert.getDialogPane().setExpandableContent(new ScrollPane(new TextArea(sw.toString())));
        return alert;
    }
}
