package chaosctrl.pdf_viewer.Controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Base64;

import org.apache.commons.io.IOUtils;
import org.tinylog.Logger;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Worker;
import javafx.concurrent.Worker.State;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;

public class PdfController {

    private File file;

    private int pageNumber = 1;

    private float pageScale = 1.5f;

    @FXML
    private WebView webView;

    @FXML
    public void initialize() {
        Logger.debug("Initializing controller");
        VBox.setVgrow(webView, Priority.ALWAYS);
    }

    @FXML
    void onNextPage(ActionEvent event) {
        pageNumber++;
        drawPdfPage();
    }

    @FXML
    void onPrevPage(ActionEvent event) {
        pageNumber--;
        if (pageNumber < 1) {
            pageNumber = 1;
        }
        drawPdfPage();
    }

    /**
     * Loads PDF and starts WebView.
     * 
     * @param file PDF file
     * @throws Exception If WebEngine has been stopped.
     */
    public void loadPdf(File file) throws Exception {
        this.file = file;
        WebEngine engine = webView.getEngine();
        String url = getClass().getResource("/viewer/index.html").toExternalForm();
        // String url = getClass().getResource("/viewer/index.html").toURI().toString(); // alternative
        engine.setJavaScriptEnabled(true);
        engine.load(url);
        // ChangeListener to wait for page load to finish
        ChangeListener<? super State> listener = new ChangeListener<>() {
            @Override
            public void changed(ObservableValue<? extends State> observable, State oldValue, State newValue) {
                if (newValue == Worker.State.SUCCEEDED) {
                    Logger.debug("WebView load finished successfully.");
                    pageNumber = 1;
                    drawPdfPage();
                } else {
                    Logger.error("WebView load finished with errors.");
                }
                // Remove listener to prevent it from being called again
                engine.getLoadWorker().stateProperty().removeListener(this);
            }
        };
        engine.getLoadWorker().stateProperty().addListener(listener);
    }

    /**
     * Draws specific PDF page on WebView.
     * 
     * @param page
     */
    private void drawPdfPage() {
        FileInputStream stream = null;
        try {
            stream = new FileInputStream(file);
            byte[] data = IOUtils.toByteArray(stream);
            String base64 = Base64.getEncoder().encodeToString(data);

            webView.getEngine().executeScript("openPdf('" + base64 + "', " + pageNumber + ", " + pageScale + ")");
        } catch (Exception e) {
            Logger.error(e);
            MsgController.createAlertForException(e).showAndWait();
        } finally {
            if (stream != null) {
                try {
                    stream.close();
                } catch (IOException e) {
                    Logger.error(e);
                    MsgController.createAlertForException(e).showAndWait();
                }
            }
        }
    }

    /**
     * Unloads WebView.
     */
    public void unloadAndClose()
    {
        WebEngine engine = webView.getEngine();
        engine.load(null);
    }
}
