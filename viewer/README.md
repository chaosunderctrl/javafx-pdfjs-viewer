# JavaScript PDF Viewer

The viewer component uses esbuild to bundle the Javascript code. There is not
much custom JavaScript so it's not quite necessary. The Gradle build does the
heavy lifting in collecting all assets and packing it into a single JAR. Important
side-note on PDF.js: the JavaFX WebView does only seem to work with the legacy
build of PDF.js. 
