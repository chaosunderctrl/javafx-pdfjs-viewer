var pdfjsLib = window['pdfjs-dist/build/pdf'];

pdfjsLib.GlobalWorkerOptions.workerSrc = 'pdf.worker.js';

/**
 * Opens base64 encoded PDF file.
 * 
 * @param {string} base64Data 
 * @param {int} pageNumber
 * @param {int} scale
 */
var openPdf = function (base64Data, pageNumber, scale) {
  var arr = base64ToArrayBuffer(base64Data);
	var loadingTask = pdfjsLib.getDocument(arr);
	loadingTask.promise.then(function(pdf) {
    pdf.getPage(pageNumber).then(function(page) {
      var viewport = page.getViewport({scale: scale});
      var canvas = document.getElementById('pdf-canvas');
      var context = canvas.getContext('2d');
      canvas.height = viewport.height;
      canvas.width = viewport.width;
  
      var renderContext = {
        canvasContext: context,
        viewport: viewport
      };
      var renderTask = page.render(renderContext);
      renderTask.promise.then(function () {
        base64Data = null;
      });
    });
	}, function (reason) {
	  console.error(reason);
	});
};

function base64ToArrayBuffer(base64) {
  var binary_string = window.atob(base64);
  var len = binary_string.length;
  var bytes = new Uint8Array( len );
  for (var i = 0; i < len; i++)        {
    bytes[i] = binary_string.charCodeAt(i);
  }
  return bytes.buffer;
}
