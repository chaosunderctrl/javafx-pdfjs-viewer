require('esbuild').build({
    entryPoints: ['src/main.js'],
    bundle: false,
    outdir: 'build/viewer',
    define: {
        "process.env.NODE_ENV": JSON.stringify("development"),
    },
});
