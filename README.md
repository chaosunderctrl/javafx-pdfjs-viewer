# PDF viewer with JavaFX and PDF.js

This is a test project I've created where I tried to build a Java PDF viewer
with current technologies. Basically I wanted to use JavaFX and Gradle and figured
[PDF.js](https://mozilla.github.io/pdf.js/) from Mozilla could be used for PDF rendering.

The Gradle project is separated in two sub-projects where `app` contains the JavaFX
part and `viewer` the JavaScript part including PDF.js.

The `viewer` sub-project uses a Node.js plugin for Gradle and is therefore able to
load the current PDF.js distribution from the NPM repository. When build it packs
all web assets into a JAR which is loaded at runtime from the JavaFX project.

The `app` sub-project is a simple JavaFX app. When a PDF file is
opened, it creates a WebView component. The component then loads the `viewer`
and after that the `app` sends the PDF as Base64 encoded string to the `viewer`
which then will be decoded and drawn inside a HTML canvas element.

The test project can be executed via Gradle with `gradle app:run`

I stopped developing this app further when I realized, that a WebView running
PDF.js does consume quite a lot of memory. I observed the same issue in Firefox
but in JavaFX it seems to be even worse. On my opinion this approach might only
be suitable for simple needs but not for building a full PDF viewer.

I must admit I stopped before doing any optimizations. One big weak point is the
complete transfer of the PDF document from the Java app to the JavaScript viewer
on every page navigation.
